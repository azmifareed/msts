import express from "express";
import bodyParser from "body-parser";
import cors from "cors";

// Controllers (route handlers)
import * as userController from "./controllers/user";

const corsOptions = {
  origin: '*'
}

const server = express();

server.use(cors(corsOptions));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.get('/users', userController.users);
server.get('/users/:id', userController.user);
server.post('/users/create', userController.create);
server.put('/users/:id', userController.update);

server.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});