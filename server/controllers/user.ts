import { Request, Response } from "express";

import User from '../models/user';
/* GET home page. */

const dummyUsers = [
    new User(1, 'John', 'Doe', '23 Blues Rd Brighton 3002', '23 Blues Rd Brighton 3002'),
    new User(2, 'Jane', 'Doe', '24 Blues Rd Brighton 3002', '24 Blues Rd Brighton 3002'),
    new User(3, 'Jack', 'Doe', '25 Blues Rd Brighton 3002', '25 Blues Rd Brighton 3002'),
];

export let users = (req: Request, res: Response) => {
    res.json(dummyUsers);
};

export let user = (req: Request, res: Response) => {

    let currentUser = dummyUsers.find(u => u.id === parseInt(req.params.id));
    if (currentUser != undefined) {
        res.json(currentUser);
    } else {
        res.status(404).send({ error: 'User not found' })
    }
};

export let create = (req: Request, res: Response) => {
    let user = new User(req.body.id, req.body.firstName, req.body.lastName, req.body.billingAddress, req.body.deliveryAddress);
    res.json(user);
};

export let update = (req: Request, res: Response) => {
    let currentUser = dummyUsers.find(u => u.id === parseInt(req.params.id));
    if (currentUser != undefined) {
      currentUser.firstName = req.body.firstName;
      currentUser.lastName = req.body.lastName;
      currentUser.billingAddress = req.body.billingAddress;
      currentUser.deliveryAddress = req.body.deliveryAddress;
      res.json(currentUser);
    } else {
        res.status(404).send({ error: 'User not found' })
    }
};

