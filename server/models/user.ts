export default class User {
  id: Number;
  firstName: String;
  lastName: String;
  deliveryAddress: String;
  billingAddress: String;

  constructor(id: Number, firstName: String, lastName: String, deliveryAddress: String, billingAddress: String) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.deliveryAddress = deliveryAddress;
    this.billingAddress = billingAddress;
  }

  getFirstName() {
      return this.firstName;
  }
  getLastName() {
    return this.lastName;
  }
  getDeliveryAddress() {
    return this.deliveryAddress;
  }
  getBillingAddress() {
    return this.billingAddress;
  }
}