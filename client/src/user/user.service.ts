import axios from 'axios';

import { User } from './user';

const api = 'http://localhost:3000';

class UserService {
  getUsers() {
    return axios.get<User[]>(`${api}/users`);
  }
  getUser(id: String) {
    return axios.get<User[]>(`${api}/users/${id}`);
  }
  updateUser(user: User) {
    return axios.put(`${api}/users/${user.id}`, user);
  }
}

export const userService = new UserService();
