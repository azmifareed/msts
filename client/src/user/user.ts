export class User {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public deliveryAddress: string,
    public billingAddress: string,
  ) {}
}
